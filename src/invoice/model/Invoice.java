package invoice.model;

import java.util.ArrayList;
import java.util.Calendar;


public class Invoice {

	private Calendar date;
	private Calendar paymentDate;
	private int daysToPayment;
	private String number;
	private ArrayList<InvoicePosition> listOfPositions=
		new ArrayList<InvoicePosition>();
	
	public Invoice()
	{
		date = Calendar.getInstance();
		paymentDate = Calendar.getInstance();
	}
	
	public int getDaysToPayment() {
		return daysToPayment;
	}

	public void setDaysToPayment(int daysToPayment) {
		this.daysToPayment = daysToPayment;
		paymentDate.add(Calendar.DAY_OF_MONTH, daysToPayment);
	}

	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public Calendar getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Calendar paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public void addInvoicePosition(InvoicePosition position)
	{
		listOfPositions.add(position);
	}
	
	public void removeInvoicePosition(InvoicePosition position)
	{
		listOfPositions.remove(position);
	}
	
	public void addInvoicePosition(Product product)
	{
		listOfPositions.add(new InvoicePosition(product));
	}
	
	public InvoicePosition getPosition(int number)
	{
		return listOfPositions.get(number);
	}
	
	public double getNetAmount()
	{
		double result = 0.0;
		
		for(InvoicePosition position : listOfPositions)
		{
			result+=position.getNetAmount();
		}
		
		return result;
	}
	
	/**
	 * Metoda obliczająca wartość kwoty brutto wszystkich produktów na fakturze
	 * @return
	 * zsumowana wartość kwoty brutto
	 */
	public double getGrossAmount()
	{
		double result = 0.0;
		
		for(InvoicePosition position : listOfPositions)
		{
			result+=position.getGrossAmount();
		}
		return result;
	}
	
	public double getVat()
	{
		double result = 0.0;
		for(InvoicePosition position : listOfPositions)
		{
			result+=position.getVatAmount();
		}
		return result;
	}
}
