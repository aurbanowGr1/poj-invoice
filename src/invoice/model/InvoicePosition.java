package invoice.model;

public class InvoicePosition {
	
	private Product product;
	private int quantity;
	private double vat;
	
	public InvoicePosition(Product product)
	{
		this.product = product;
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		if(product == null) return;
		this.product = product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	
	public double getNetAmount()
	{
		return quantity * product.getPrice();
	}
	
	public double getGrossAmount()
	{
		return (1 + vat) * getNetAmount();
	}
	
	public double getVatAmount()
	{
		return vat * getNetAmount();
	}

}
