package invoice.model;

public class Main {

	public static void main(String[] args) {
		
		Invoice invoice = new Invoice();
		invoice.setDaysToPayment(7);
		invoice.setNumber("1/4/2015");
		
		Product bmw = new Product();
		bmw.setName("Samochód osobowy BMW");
		bmw.setPrice(15000);
		bmw.setUnit("sztuka");
		
		Product aro = new Product();
		aro.setName("Samochód Aro");
		aro.setPrice(1000);
		aro.setUnit("sztuka");
		
		invoice.addInvoicePosition(aro);
		invoice.addInvoicePosition(bmw);

		InvoicePosition aroPosition = invoice.getPosition(0);
		InvoicePosition bmwPosition = invoice.getPosition(1);
		
		aroPosition.setQuantity(2);
		aroPosition.setVat(0.23);
		bmwPosition.setQuantity(5);
		bmwPosition.setVat(0.23);
		

		System.out.println("Data wystawienia: "+ invoice.getDate().getTime());
		System.out.println("Data wystawienia: "+ invoice.getPaymentDate().getTime());
		System.out.println("kwota netto "+invoice.getNetAmount());
		System.out.println("kwota brutto "+invoice.getGrossAmount());
		System.out.println("podatek vat "+invoice.getVat());
		System.out.println();
		

	}

}
